import { memo, useState } from 'react';
import styles from './style.module.scss';

function Image({ src, alt, srcFull }) {
  const [isActive, setIsActive] = useState(false);
  const active = () => {
    setIsActive(true);
    document.body.style = 'height:100vh; overflow:hidden';
  }
  const deactivate = (e) => {
    if (!e.target.matches('[class*=popup__image]')) {
      setIsActive(false);
      document.body.removeAttribute('style');
    }
  }

  return (
    <li>
      <img className={styles.image} src={src} alt={alt} onClick={active} />

      {isActive &&
        <div className={styles.popup} onClick={deactivate}>
          <button className={styles.close}><img src='/images/close.svg' alt='close popup icon' /></button>
          <img className={styles['popup__image']} src={srcFull} alt={alt} />
        </div>
      }
    </li>
  )
}

export default memo(Image);