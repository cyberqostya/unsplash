import styles from './style.module.scss';

export default function Input({ phrase, typing, clear, isFirstPressSubmit }) {

  function preventScroll() {
    if (!isFirstPressSubmit && window.innerWidth <= 768) {
      document.querySelector('form').style = 'opacity: 0;';
      setTimeout(() => {
        window.scrollTo(0, 0);
        document.querySelector('form').removeAttribute('style');
      }, 200)
    }
  }

  return (
    <label className={styles.wrapper}>
      <img src='/images/magnifier.svg' alt='search icon' />
      <input minLength={2} required value={phrase} type='text' placeholder='Телефоны, яблоки, груши...' className={styles.input} onChange={typing} onFocus={preventScroll} />
      <img src='/images/clear.svg' alt='search icon' onClick={clear} />
    </label>
  )
}