import { memo, useEffect, useState } from "react";
import styles from './style.module.scss';

function AfterText({ apiResponseStatus }) {
  const [text, setText] = useState('');

  useEffect(() => {
    if (apiResponseStatus === 200) {
      setText('К сожалению, поиск не дал результатов');
    } else if (apiResponseStatus === 400) {
      setText('Ошибка запроса, попробуйте снова');
    } else if (apiResponseStatus === 403) {
      setText('Превышен лимит запросов: 50 в час в бесплатном unsplash');
    } else {
      setText('Ошибка сервера, повторите попытку через некоторое время');
    }

  }, [])

  return <p className={styles.text}>{text}</p>;
}

export default memo(AfterText);