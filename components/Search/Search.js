import Input from '@com/Input/Input';
import styles from './style.module.scss';
import Button from '@com/Button/Button';
import { memo } from 'react';

function Search({
  searchPhrase,
  typeSearchPhrase,
  clearSearchPhrase,
  getImages,
  isFirstPressSubmit
}) {

  return (
    <form className={`${styles.search} ${isFirstPressSubmit ? styles['search_submit'] : ''}`} onSubmit={getImages}>
      <Input phrase={searchPhrase} typing={typeSearchPhrase} clear={clearSearchPhrase} isFirstPressSubmit={isFirstPressSubmit} />
      <Button>Искать</Button>
    </form>
  )
}

export default memo(Search);