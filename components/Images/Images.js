import styles from './style.module.scss';
import Image from '@com/Image/Image';

function Images({ displayedImages }) {
  return (
    <ul className={styles.images}>
      {displayedImages.map((i, key) => <Image key={key} src={i.urls.small} alt={i.alt_description} srcFull={i.urls.regular} />)}
    </ul>
  )
}

export default Images;