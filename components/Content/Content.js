'use client';

import Search from '@com/Search/Search';
import styles from './style.module.scss';
import { useState, useRef, useEffect, useCallback } from 'react';
import AfterText from '@com/AfterText/AfterText';
import Images from '@com/Images/Images';
import detectLanguage from '@/helpers/detectLanguage';
import { url, PER_PAGE } from '@/helpers/variables';

export default function Content() {
  const [isLoading, setIsLoading] = useState(false);
  const [apiResponseStatus, setApiResponseStatus] = useState(0);
  const [apiResponse, setApiResponse] = useState(null);
  const [displayedImages, setDisplayedImages] = useState([]);
  const [isFirstPressSubmit, setIsFirstPressSubmit] = useState(false);

  const [searchPhrase, setSearchPrase] = useState('');
  const typeSearchPhrase = (e) => { setSearchPrase(e.target.value) }
  const clearSearchPhrase = () => { setSearchPrase('') }

  const [pageEffect, setPageEffect] = useState(1);
  const page = useRef(1);

  const fetchURL = useRef('');

  const getImages = async (e) => {
    e.preventDefault();

    !isFirstPressSubmit && setIsFirstPressSubmit(true);
    setIsLoading(true);
    setDisplayedImages([]);

    fetchURL.current = url + `&query=${encodeURI(searchPhrase)}&lang=${detectLanguage(searchPhrase)}&per_page=${PER_PAGE}`;

    fetch(fetchURL.current)
      .then((res) => {
        setApiResponseStatus(res.status);
        if (res.ok) return res.json();
      })
      .then(res => {
        // console.log(res);
        setApiResponse(res);
        setDisplayedImages(res.results);
      })
      .catch((err) => console.log(err.message))
      .finally(() => {
        setIsLoading(false);
        setPageEffect(1);
      })
  }

  const loadMoreImages = async () => {
    // console.log(page.current);
    setIsLoading(true);

    fetch(fetchURL.current + `&page=${page.current}`)
      .then((res) => {
        if (res.ok) return res.json();
      })
      .then(res => {
        // console.log(res);
        setDisplayedImages([...displayedImages, ...res.results]);
      })
      .catch((err) => console.log(err.message))
      .finally(() => {
        setIsLoading(false);
      })
  }

  // Изменение страницы от замкнутой функции с неправильным useState
  useEffect(() => {
    // Произошёл первый запрос
    if (!isFirstPressSubmit) return;

    // Есть ли еще фото для подгрузки
    if (apiResponse.total_pages <= 1 || displayedImages.length >= apiResponse.total) return;

    page.current = pageEffect;
    loadMoreImages();
  }, [pageEffect])

  // Проверка, доскроллил ли пользователь до низа
  useEffect(() => {
    const threshold = 300;
    let isExecuting = false;
    const scrollHandler = async () => {
      // Долистали до конца
      if ((window.innerHeight + document.documentElement.scrollTop >= document.documentElement.scrollHeight - threshold) && !isExecuting) {

        // console.log('executing');
        isExecuting = true;
        setPageEffect((prevPage) => prevPage + 1);

        await new Promise(res => setTimeout(res, 800));
        isExecuting = false;
      }
    };

    window.addEventListener('scroll', scrollHandler);
    return () => { window.removeEventListener('scroll', scrollHandler); }
  }, []);

  return (
    <main className={`${styles.main} ${isFirstPressSubmit ? styles['main_submit'] : ''}`}>

      <Search
        searchPhrase={searchPhrase}
        typeSearchPhrase={typeSearchPhrase}
        clearSearchPhrase={clearSearchPhrase}
        getImages={getImages}
        isFirstPressSubmit={isFirstPressSubmit}
      />

      {displayedImages.length > 0 && <Images displayedImages={displayedImages} />}

      {isFirstPressSubmit && !isLoading && (apiResponseStatus !== 200 || apiResponseStatus === 200 && apiResponse.total === 0) && <AfterText apiResponseStatus={apiResponseStatus} />}

    </main>
  )
}